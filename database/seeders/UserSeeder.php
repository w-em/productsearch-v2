<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name' => 'sj@w-em.com',
            'email' => 'sj@w-em.com',
            'password' => 'sommer83'
        ]);

        \App\Models\User::create([
            'name' => 'stefan.thiel@se.com',
            'email' => 'stefan.thiel@se.com',
            'password' => 'stefan.thiel@se.com'
        ]);

        \App\Models\User::create([
            'name' => 'julia.kwasny@se.com',
            'email' => 'julia.kwasny@se.com',
            'password' => 'julia.kwasny@se.com'
        ]);
    }
}
