<?php

namespace Database\Seeders;

use App\Models\FileDirectory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class FileStorageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (['Produktbilder', 'Massdarstellung', 'Prinzipdarstellung', 'Montageanleitung'] as $name) {
            FileDirectory::create([
                'parent_id' => 0,
                'identifier' => Str::slug($name),
                'name' => $name,
            ]);
        }
    }
}
