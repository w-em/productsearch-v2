<?php

namespace Database\Seeders;

use App\Models\File;
use App\Models\FileDirectory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class FileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $massdarstellung = FileDirectory::where([['identifier', '=', 'massdarstellung']])->first();
        $prinzipdarstellung = FileDirectory::where([['identifier', '=', 'prinzipdarstellung']])->first();
        $produktbilder = FileDirectory::where([['identifier', '=', 'produktbilder']])->first();

        $fileArray = Storage::disk('seed')->allFiles();

        foreach ($fileArray as $fileStr) {
            $info = pathinfo($fileStr);

            $fileSize = Storage::disk('seed')->size($fileStr);
            $mimeType = Storage::disk('seed')->mimeType($fileStr);

            $dirSplit = explode('/', $info['dirname']);
            $directoryId = null;
            $directoryIdentifier = null;

            switch (strtolower($dirSplit[0])) {
                case 'maßdarstellung':
                    $directoryId = $massdarstellung->id;
                    $directoryIdentifier = $massdarstellung->identifier;
                    break;
                case 'prinzipdarstellung':
                    $directoryId = $prinzipdarstellung->id;
                    $directoryIdentifier = $prinzipdarstellung->identifier;
                    break;
                case 'produktbilder':
                    $directoryId = $produktbilder->id;
                    $directoryIdentifier = $produktbilder->identifier;
                    break;
                default:
                    $directoryId = null;
                    break;
            }

            if (is_null($directoryId)) {
                continue;
            }

            $fileIdentifier = implode('/', [$directoryIdentifier, strtolower($info['basename'])]);

            $file = File::updateOrCreate(
                [
                    'identifier' => $fileIdentifier,
                ],
                [
                    'file_directory_id' => $directoryId,
                    'identifier' => $fileIdentifier,
                    'identifier_hash' => sha1(implode('/', [$directoryIdentifier, strtolower($info['basename'])])),
                    'name' => strtolower($info['basename']),
                    'mime_type' => $mimeType,
                    'size' => $fileSize,
                    'alternative' => strtolower($info['filename']),
                    'extension' => strtolower($info['extension']),
                ]
            );

            $fileIdentifierInfo = pathinfo($fileIdentifier);

            if (!\File::exists(storage_path('app/public/images/' . $fileIdentifierInfo['dirname']))) {
                \File::makeDirectory(storage_path('app/public/images/' . $fileIdentifierInfo['dirname']));
            }

            \File::copy(storage_path('seed/' . $fileStr), storage_path('app/public/images/' . $fileIdentifier));
        }
    }
}
