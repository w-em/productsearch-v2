<?php

namespace Database\Seeders;

use App\Services\ImportService;
use Illuminate\Database\Seeder;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('seeders/attributes.csv');
        ImportService::importAttributeFromCsvFile($file);
    }
}
