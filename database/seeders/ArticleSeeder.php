<?php

namespace Database\Seeders;

use App\Jobs\ImportArticlesJob;
use App\Services\ImportService;
use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ImportArticlesJob::dispatch('Kleinverteiler', 0, 1000, true);
        ImportArticlesJob::dispatch('Feldverteiler', 0, 1000, true);
        ImportArticlesJob::dispatch('ZP', 0, 1000, true);
    }
}
