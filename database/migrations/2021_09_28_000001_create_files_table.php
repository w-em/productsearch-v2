<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_directories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('parent_id');
            $table->string('identifier')->unique();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('file_directory_id')->index();
            $table->string('identifier')->index();
            $table->string('identifier_hash')->index();
            $table->string('extension');
            $table->string('mime_type');
            $table->string('name');
            $table->unsignedBigInteger('size');
            $table->string('alternative');
            $table->timestamps();
        });

        Schema::create('file_references', function (Blueprint $table) {
            $table->id();
            $table->string('tablenames');
            $table->string('field_name');
            $table->unsignedBigInteger('id_local'); // file
            $table->unsignedBigInteger('id_foreign'); // article_id
            $table->unsignedInteger('sorting');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
        Schema::dropIfExists('file_references');
        Schema::dropIfExists('file_directories');
    }
}
