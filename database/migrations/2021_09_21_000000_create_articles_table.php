<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('vendor')->index();
            $table->string('product_family')->index();
            $table->string('article_number')->index();
            $table->text('short_description')->nullable();
            $table->text('long_description')->nullable();
            $table->text('keywords')->nullable();
            $table->string('ean')->nullable()->index();
            $table->string('availability')->nullable()->index();
            $table->string('fee_number')->nullable()->index();
            $table->string('product_type_lvl4')->nullable()->index();
            $table->string('product_type_lvl5')->nullable()->index();
            $table->string('country_of_origin')->nullable()->index();
            $table->float('price', 8, 2)->nullable()->index();
            $table->string('slug')->index();
            $table->boolean('active')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
