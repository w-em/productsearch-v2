<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleAttributeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_attributes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('article_id')->index();
            $table->unsignedBigInteger('attribute_id')->index();
            // $table->foreignId('article_id')->constrained('articles')->onDelete('cascade');
            // $table->foreignId('attribute_id')->constrained('attributes')->onDelete('cascade');
            $table->unsignedBigInteger('attribute_value_id')->index();

            $table->index(['article_id', 'attribute_id'], 'idx_a_at');
            $table->index(['article_id', 'attribute_id', 'attribute_value_id'], 'idx_a_at_av');
            $table->index(['attribute_id', 'attribute_value_id'], 'idx_at_av');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_attributes');
    }
}
