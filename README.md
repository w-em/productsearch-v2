## Install
```mkdir abn-productsearch && cd abn-productsearch```

```git clone git@bitbucket.org:w-em/productsearch-v2.git .```

```ddev config```

- Project Name: `abn-productsearch`
- Docroot Location: `public`  
- Project Type: `laravel`

## live data FTP
* `host: 212.100.236.193`
* `username: sjaeger`
* `pw: R0WlXdc6VnrYTPq`

```ddev start && ddev launch```

## Quickstart install and import locally
* `git clone git@bitbucket.org:w-em/productsearch-v2.git .`
* `ddev start`
* `[ ! -d ".ddev/.downloads" ] && mkdir -p ".ddev/.downloads"`
* `ddev composer install`
* `ddev exec cp .env-local .env`
* `ddev exec php artisan migrate`
* `ddev exec php artisan db:seed`
* `ddev exec php artisan wem:import`
* `ddev exec php artisan queue:listen`
* `ddev exec php artisan cache:clear`

## Install node
* `ddev ssh`
* `cd resources/frontend`
* `npm install`
* `npm run dev`

## Commands
```ddev exec php artisan```
```ddev exec php artisan route:list```


## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

You may also try the [Laravel Bootcamp](https://bootcamp.laravel.com), where you will be guided through building a modern Laravel application from scratch.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains thousands of video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.
