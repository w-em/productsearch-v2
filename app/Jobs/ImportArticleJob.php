<?php

namespace App\Jobs;

use App\Models\Article;
use App\Models\ArticleAttribute;
use App\Models\ArticleCategory;
use App\Models\AttributeValue;
use App\Models\Category;
use App\Models\FileDirectory;
use App\Models\FileReference;
use App\Services\FileMakerService;
use App\Services\ImportService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ImportArticleJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * The maximum number of exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 180;

    /**
     * @var null
     */
    protected $curlResult = null;
    protected $data = null;

    /**
     * @var bool
     */
    protected $force = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($curlResult, $force = true)
    {
        $this->curlResult = $curlResult;
        $this->force = $force;
    }

    /**
     * Handles the import of an article, including upserting the article, its categories, attributes, and images.
     *
     * This method is responsible for the following tasks:
     * - Parsing the article data from the curl result
     * - Mapping the data to the appropriate article, category, attribute, and image fields
     * - Upserting the article, categories, attributes, and images
     *
     * @return void
     */
    public function handle()
    {
        $this->data = (array)$this->curlResult->fieldData;

        $article = [];
        $attributes = [];
        $images = [];
        $categories = [];

        $mainKeys = array_keys(Article::FIELD_MAPPING_MAIN);
        $categoryKeys = array_keys(Article::FIELD_MAPPING_CATEGORY);
        $imageKeys = array_keys(Article::FIELD_MAPPING_IMAGES);
        $attributeKeys = array_keys(Article::FIELD_MAPPING_ATTRIBUTES);

        foreach($this->data as $key => $value) {

            $value = trim($value);

            if (in_array(strtolower($value), Article::FIELD_VALUE_EMPTY)) {
                continue;
            } elseif (in_array($key, Article::FIELD_IGNORE)) {
                continue;
            } elseif (empty($value))  {
                continue;
            } elseif (in_array($key, Article::FIELD_DATES)) {
                $value = new Carbon($value);
            } elseif (in_array($key, Article::FIELD_INTEGERS)) {
                $value = (int) $value;
            } elseif (in_array($key, Article::FIELD_FLOATS)) {
                $value = (float)$value;
            } else {
                $value = nl2br($value);
            }

            if (in_array($key, $categoryKeys)) {
                $categories[Article::FIELD_MAPPING_CATEGORY[$key]] = $value;
            }

            if (in_array($key, $mainKeys)) {
                $article[Article::FIELD_MAPPING_MAIN[$key]] = $value;
            }

            if (in_array($key, $imageKeys)) {
                $images[] = [
                    'key' => $key,
                    'category' => Article::FIELD_MAPPING_IMAGES[$key],
                    'value' => $value
                ];
            }

            if (in_array($key, $attributeKeys)) {
                $attributes[Article::FIELD_MAPPING_ATTRIBUTES[$key]] = $value;
            }

            if (
                !in_array($key, $categoryKeys)
                && !in_array($key, $mainKeys)
                && !in_array($key, $imageKeys)
                && !in_array($key, $attributeKeys)
            ) {
                Log::channel('database')->info("ImportArticleJob: (Artikel_nummer: ".$article['article_number'].")Attribute not found ( AttributeKey: " . $key . " ); ");
            }
        }

        $article['active'] = 1;
        $article['slug'] = Str::slug($article['article_number']);

        $articleModel = $this->upsertArticle($article);

        $this->upsertCategories($articleModel, $categories);

        $this->upsertAttributes($articleModel, $attributes);
        $this->upsertImages($articleModel, $images);

    }

    /**
     * Upserts an article in the database.
     *
     * This method will either create a new article or update an existing one based on the article number.
     *
     * @param array $data An associative array containing the article data to be upserted.
     * @return Article The upserted article model instance.
     */
    protected function upsertArticle($data) {

        return Article::updateOrCreate(
            ['article_number' => $data['article_number']],
            $data
        );
    }

    /**
     * Upserts images associated with an article.
     *
     * This method downloads and saves images for an article, and creates or updates the corresponding file references in the database.
     *
     * @param Article $article The article to associate the images with.
     * @param array $images An array of image data, including the image category, key, and value.
     * @return bool True if the image upsert was successful, false otherwise.
     */
    protected function upsertImages(Article $article, array $images) : bool {

        $sortings = [];
        $massdarstellung = FileDirectory::where([['identifier', '=', 'massdarstellung']])->first();
        $prinzipdarstellung = FileDirectory::where([['identifier', '=', 'prinzipdarstellung']])->first();
        $produktbilder = FileDirectory::where([['identifier', '=', 'produktbilder']])->first();
        $montageanleitung = FileDirectory::where([['identifier', '=', 'montageanleitung']])->first();

        FileReference::where([
            ['id_foreign', '=', $article->id]
        ])->delete();

        foreach ($images as $image) {
            $urlArr = ['https://dbserv.ddns.net:444/Shared'];

            if (ucfirst($image['category']) === 'Massdarstellung') {
                $urlArr[] = 'Maßdarstellung';
            } else {
                $urlArr[] = ucfirst($image['category']);
            }

            if (ucfirst($image['category']) !== 'Montageanleitung') {
                $urlArr[] = $image['key'];
            }

            $urlArr[] = $image['value'];

            $filePath = storage_path('app/public/images/' . $image['category']);
            $targetFile = $filePath . '/'. strtolower($image['value']);

            $forceDownload = $this->force;

            if ($forceDownload === true || !file_exists($targetFile)) {

                $url = implode(DIRECTORY_SEPARATOR, $urlArr) . '?login=download:s';
                $downloadResult = FileMakerService::downloadFile($url);

                if ($downloadResult['status'] !== 200) {

                    Log::channel('database')->info("ImportArticleJob: Image not found ( ArticleNo: " . $article->article_number . ";URL: " . $url . " ); ");
                    File::delete(storage_path('app/download') . '/'. $image['value']);
                    continue;
                }
                $image['download'] = $downloadResult;

                if (!is_dir(storage_path('app/public/images'))) {
                    Storage::makeDirectory('public/images');
                }

                if (!is_dir($filePath)) {
                    Storage::makeDirectory('public/images/' . $image['category']);
                }

                if (file_exists($image['download']['filepath'])) {
                    File::move($image['download']['filepath'], $targetFile);
                }
            }

            $image['url'] = implode(DIRECTORY_SEPARATOR, $urlArr);

            if (!key_exists($image['category'], $sortings)) {
                $sortings[$image['category']] = 0;
            } else {
                $sortings[$image['category']]++;
            }

            $image['sorting'] = $sortings[$image['category']];

            $info = pathinfo($targetFile);
            $fileSize = filesize($targetFile);
            $mimeType = false;

            try {
                $mimeType = $info['extension'];
            } catch(\Exception $exception) {
                throw new \Exception('invalid mime type: ' . $targetFile);
            }


            switch (strtolower($image['category'])) {
                case 'massdarstellung':
                    $directoryId = $massdarstellung->id;
                    $directoryIdentifier = $massdarstellung->identifier;
                    break;
                case 'prinzipdarstellung':
                    $directoryId = $prinzipdarstellung->id;
                    $directoryIdentifier = $prinzipdarstellung->identifier;
                    break;
                case 'produktbilder':
                    $directoryId = $produktbilder->id;
                    $directoryIdentifier = $produktbilder->identifier;
                    break;
                case 'montageanleitung':
                    $directoryId = $montageanleitung->id;
                    $directoryIdentifier = $montageanleitung->identifier;
                    break;
                default:
                    $directoryId = null;
                    break;
            }

            if (is_null($directoryId)) {
                throw new \Exception('directory not found');
            }

            $fileIdentifier = implode('/', [$directoryIdentifier, strtolower($info['basename'])]);

            $file = \App\Models\File::updateOrCreate(
                [
                    'identifier' => $fileIdentifier,
                ],
                [
                    'file_directory_id' => $directoryId,
                    'identifier' => $fileIdentifier,
                    'identifier_hash' => sha1(implode('/', [$directoryIdentifier, strtolower($info['basename'])])),
                    'name' => strtolower($info['basename']),
                    'mime_type' => $mimeType,
                    'size' => $fileSize,
                    'alternative' => strtolower($info['filename']),
                    'extension' => strtolower($info['extension']),
                ]
            );

            $ref = FileReference::updateOrCreate([
                'id_foreign' => $article->id,
                'field_name' => $image['category'],
                'tablenames' => 'articles',
                'sorting' => $image['sorting'],
            ], [
                'id_local' => $file->id
            ]);
        }

        return true;
    }

    /**
     * Upserts the categories for the given article.
     *
     * This method first deletes all existing article-category associations for the given article.
     * It then iterates through the provided category data, creating or updating the category and
     * associating it with the article.
     *
     * @param Article $article The article to update the categories for.
     * @param array $data The category data to upsert.
     * @return void
     */
    protected function upsertCategories(Article $article, $data) {
        ArticleCategory::where([
            ['article_id', '=', $article->id]
        ])->delete();

        $category = null;
        foreach ($data as $key => $val) {

            if (empty(trim($val))) {
                continue;
            }

            $parentId = 0;
            $slug = \Str::slug($val);
            if (!is_null($category)) {
                $parentId = $category->id;
                $slug = $category->slug . '-' . \Str::slug($val);
            }

            $category = Category::updateOrCreate(
                [
                    'title' => $val,
                    'parent_id' => $parentId
                ],
                [
                    'title' => $val,
                    'parent_id' => $parentId,
                    'slug' => $slug
                ]
            );

            ArticleCategory::updateOrCreate([
                'article_id' => $article->id,
                'category_id' => $category->id,
            ], [
                'article_id' => $article->id,
                'category_id' => $category->id,
            ]);
        }
    }

    /**
     * This method first deletes all existing article-attribute associations for the given article.
     * It then iterates through the provided attribute data, creating or updating the attribute value and
     * associating it with the article.
     *
     * @param Article $article The article to update the attributes for.
     * @param array $attributes The attribute data to upsert.
     * @return void
     */
    protected function upsertAttributes(Article $article, $attributes) {

        ArticleAttribute::where([
            ['article_id', '=', $article->id]
        ])->delete();

        foreach ($attributes as $key => $value) {
            $attributeValue = AttributeValue::updateOrCreate(
                [
                    'attribute_id' => (int) $key,
                    'value' => $value
                ],
                [
                    'attribute_id' => (int) $key,
                    'value' => $value
                ]
            );

            ArticleAttribute::create([
                'article_id' => $article->id,
                'attribute_id' => (int) $key,
                'attribute_value_id' => $attributeValue->id
            ]);
        }


    }
}
