<?php

namespace App\Jobs;

use App\Models\Article;
use App\Services\ImportService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportArticlesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * The maximum number of exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 60;

    /**
     * the offset of current request
     * @var int
     */
    protected $offset = 0;

    /**
     * @var int
     */
    protected $limit = 1000;

    /**
     * @var String
     */
    protected $categoryName;

    /**
     * @var null
     */
    protected $category = null;

    /**
     * @var bool
     */
    protected $force = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($layoutName, $offset = 0, $limit = 1000, $force = false)
    {
        $this->categoryName = $layoutName;

        $this->offset = $offset;
        $this->limit = $limit;
        $this->force = $force;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $results = ImportService::getArticleList($this->offset, $this->limit, $this->categoryName);

        $count = count($results);

        if ($count > 0) {
            $offset = $this->offset + $this->limit;
            ImportArticlesJob::dispatch($this->categoryName, $offset, $this->limit, $this->force);

            foreach ($results as $result) {
                if ($result->fieldData->Artikelnummer === "1001" || $result->fieldData->Artikelnummer === "Artikelnummer") {
                    // TODO: Should we import Property Keys?
                } else {
                    ImportArticleJob::dispatch($result, $this->force);
                }

                /*
                if ($result->fieldData->Artikelnummer === "Z17110") {
                    $keys = array_keys((array) $result->fieldData);
                    $articleKeys = array_keys(array_merge(Article::FIELD_MAPPING_ATTRIBUTES, Article::FIELD_MAPPING_CATEGORY, Article::FIELD_MAPPING_MAIN, Article::FIELD_MAPPING_IMAGES));
                    $articleKeys[] = "Listenpreis";
                    $articleKeys[] = "Ausschreibungstexte";


                    $articleKeys[] = "Mit_Spannungsabgriff_und_integrierter_Vorsicherung";
                    $articleKeys[] = "Produktzertifizierungen";
                    $articleKeys[] = "Typ der �berspann_Abl_Klasse";



                    foreach($keys as $key) {
                        if (!in_array($key, $articleKeys)) {
                            dd($key);
                        }
                    }
                }*/

            }


        }


    }
}
