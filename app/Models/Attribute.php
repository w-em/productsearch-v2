<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Laravel\Scout\Searchable;
use TeamTNT\TNTSearch\Indexer\TNTIndexer;

class Attribute extends Model
{
    use Searchable;
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
        'attribute_group_id',
        'standard_filter',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        /*
        $categories = [];
        foreach ($this->categories as $category) {
            $categories[] = $category->title;
        }*/
        return [
            'id' => $this->id,
            'title' => $this->title,
            'article_numberNgrams' => utf8_encode((new TNTIndexer)->buildTrigrams($this->article_number)),
            'short_description' => $this->short_description,
            'article_number' => $this->article_number,
            // 'category' => implode(' ', $categories),
            'ean' => $this->ean,
            'keywords' => $this->keywords,
        ];
    }

    public function group() {
        return $this->hasOne(AttributeGroup::class, 'id', 'attribute_group_id');
    }
}
