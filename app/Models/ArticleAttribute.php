<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleAttribute extends Model
{

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_id',
        'attribute_id',
        'attribute_value_id',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function attribute() {
        return $this->hasOne(Attribute::class, 'id', 'attribute_id');
    }

    public function value() {
        return $this->hasOne(AttributeValue::class, 'id', 'attribute_value_id');
    }
}
