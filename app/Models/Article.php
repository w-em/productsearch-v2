<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Scout\Searchable;
use TeamTNT\TNTSearch\Indexer\TNTIndexer;

class Article extends Model
{
    use Searchable;
    use HasFactory;

    const FIELD_IGNORE = [
        'Listenpreis'
    ];

    const FIELD_MAPPING_MAIN = [
        'Artikelstatus' => 'availability',
        'Beschreibung1' => 'title',
        'Produktmarke' => 'vendor',
        'Produktfamilie' => 'product_family',
        'Artikelnummer' => 'article_number',
        'Beschreibung2' => 'short_description',
        'Langtext' => 'long_description',
        'EAN Code' => 'ean',
        'ABN_Artikelstamm_HPL::Listen_Preis_EUR' => 'price',
        'Zolltarif Nr.' => 'fee_number',
        'Created' => 'created_at',
        'Modified' => 'updated_at',
        'Ursprungsland' => 'country_of_origin',
        'Produktart_ gem. PM0 _ Level 4' => 'product_type_lvl4',
        'Produkttyp_ gem. PM0 _ Level 5' => 'product_type_lvl5'
    ];

    const FIELD_MAPPING_ATTRIBUTES = [
        "Breite _mm_" => "2001",
        "Höhe _mm_" => "2002",
        "Tiefe _mm_" => "2003",
        "Blendrahmen BxHxT _mm_" => "2004",
        "Nischenmaß BxHxT _mm_" => "2005",
        "Ausschnitt BxHxT _mm_" => "2006",
        "Bauhöhe" => "2007",
        "Feldbreite" => "2008",
        "Anzahl Reihen" => "2009",
        "Anzahl Teilungseinheiten" => "2010",
        "Ausführung Messplatz" => "2011",
        "Ausführung Wandlerteil" => "2012",
        "Anzahl der N_Klemmen" => "2013",
        "Anzahl der N_N_Klemmen" => "2014",
        "Anzahl der PE_Klemmen" => "2015",
        "Anzahl der PE_N_Klemmen" => "2016",
        "Polzahl" => "2020",
        "Beschreibung der Pole" => "2021",
        "Mit Fernmeldekontakt" => "2022",
        "Signalisierung am Gerät" => "2023",
        "Erdungssystem" => "2024",
        "Mit Spannungsabgriff und integrierter Vorsicherung" => "2025",
        "Leckstromfrei" => "2026",
        "Einsatzbereich" => "2017",
        "Farbe" => "2018",
        "RAL_Ton" => "2019",
        "Schrankmontage" => "3001",
        "Kabeleinführung" => "3002",
        "Anzahl Türen" => "3003",
        "Türmaterial" => "3004",
        "Türausführung" => "3005",
        "Türöffnungsseite" => "3006",
        "Türöffnungswinkel _°_" => "3007",
        "Anzahl Schlösser" => "3008",
        "Montagehalterung" => "3009",
        "Mauerkasten" => "3010",
        "Gerätetragschiene" => "3011",
        "Blendrahmen" => "3012",
        "Verteiler_Bodenplatte" => "3013",
        "Patch_Panel" => "3014",
        "Material" => "3017",
        "Produktzertifizierungen " => "3018",
        "Qualitätslabel" => "3019",
        "Normen" => "3015",
        "Montageart" => "3016",
        "Bemessungsspannung AC Un _V_" => "4001",
        "Bemessungsstoßspannungsfestigkeit Uimp _kV_" => "4002",
        "Nennstrom In _A_" => "4003",
        "Bemessungsbetriebstrom Inc _A_" => "4004",
        "Bemessungsstoßstromfestigkeit Icc _kA_" => "4005",
        "Bemessungstoßsstromfestigkeit Ipk _kA_" => "4006",
        "Bemessungskurzzeitstromfestigkeit Icw _1s_ _kA_" => "4007",
        "Bemessungsfrequenz fn _Hz_" => "4008",
        "RDF bei Dauerbelastung" => "4009",
        "Kurzschlussfestigkeit_Isccr" => "4016",
        "Max Ableitstoßstrom_8_20" => "4017",
        "Ableitvermögen_ka" => "4018",
        "Schutzpegel_L_N_kV" => "4019",
        "Schutzpegel_N_PE_kV" => "4020",
        "Verlustleistung _W_" => "4010",
        "Schutzklasse" => "4011",
        "IP_Schutzart" => "4012",
        "Prüfklasse" => "4021",
        "Typ der Überspann.Abl._Klasse" => "4022",
        "Stoßfestigkeitsgrad IK_Code" => "4013",
        "Überspannungskategorie" => "4014",
        "Verschmutzungsgrad nach IEC 61010_1" => "4015",
        "Umgebungstemperatur Mittelwert _°C_" => "5001",
        "Umgebungstemperatur bei Betrieb min. _°C_" => "5002",
        "Umgebungstemperatur bei Betrieb max. _°C_" => "5003",
        "Feuer Beständigkeit _°C_" => "5004",
        "Relative Luftfeuchtigkeit dauerhaft _%_" => "5005",
        "Relative Luftfeuchtigkeit kurzzeitig _%_" => "5006",
        "Verpackungseinheit _VPE_" => "6001",
        "VPE auf Palette" => "6002",
        "Verpackungsabmessungen Breite _mm_" => "6003",
        "Verpackungsabmessungen Höhe _mm_" => "6004",
        "Verpackungsabmessungen Tiefe _mm_" => "6005",
        "Verpackungsgewicht _kg_" => "6006",
        "EU_RoHS_Richtlinie" => "7001",
    ];

    const FIELD_MAPPING_CATEGORY = [
        // 'Produktart_ gem. PM0 _ Level 4' => 0,
        'Kategorie' => 0,
        'Unterkategorie2' => 1,
        'Unterkategorie3' => 2,
        'Unterkategorie4' => 3,
        'Unterkategorie5' => 4,
    ];

    const FIELD_MAPPING_IMAGES = [
        "Produkt1" => "produktbilder",
        "Produkt2" => "produktbilder",
        "Produkt3" => "produktbilder",
        "Produkt4" => "produktbilder",
        "Produkt5" => "produktbilder",
        "Produkt6" => "produktbilder",
        "Produkt7" => "produktbilder",
        "Prinzip1" => "prinzipdarstellung",
        "Prinzip2" => "prinzipdarstellung",
        "Prinzip3" => "prinzipdarstellung",
        "Prinzip4" => "prinzipdarstellung",
        "Prinzip5" => "prinzipdarstellung",
        "Prinzip6" => "prinzipdarstellung",
        "Prinzip7" => "prinzipdarstellung",
        "Maß1" => "massdarstellung",
        "Maß2" => "massdarstellung",
        "Maß3" => "massdarstellung",
        "Montageanleitung1" => "montageanleitung",
        "Montageanleitung2" => "montageanleitung",
        "Montageanleitung3" => "montageanleitung",
        "Montageanleitung4" => "montageanleitung",
        "Montageanleitung5" => "montageanleitung",
    ];

    const FIELD_DATES = [
        'Created',
        'Modified',
    ];

    const FIELD_FLOATS = [
        "Verlustleistung _W_",
        "Umgebungstemperatur Mittelwert _°C_",
        "Umgebungstemperatur bei Betrieb max. _°C_",
        "Feuer Beständigkeit _°C_",
        "Verpackungsgewicht _kg_",
    ];

    const FIELD_INTEGERS = [
        "EAN Code",
        "Zolltarif Nr.",
        "Breite _mm_",
        "Höhe _mm_",
        "Tiefe _mm_",
        "Bauhöhe",
        "Feldbreite",
        "Anzahl Teilungseinheiten",
        "RAL_Ton",
        "Bemessungsspannung AC Un _V_",
        "Bemessungsstoßspannungsfestigkeit Uimp _kV_",
        "Nennstrom In _A_",
        "Bemessungsbetriebstrom Inc _A_",
        "Bemessungsstoßstromfestigkeit Icc _A_",
        "Bemessungsstoßstromfestigkeit Ipk _kA_",
        "Bemessungskurzzeitstromfestigkeit Icw _1s_ _kA_",
        "RDF bei Dauerbelastung",
        "Kurzschlussfestigkeit Isccr _kA_",
        "Max Ableitstoßstrom _8_20 µs_ _kA_",
        "Ableitvermögen I imp _10_350 µs_ _ka_",
        "Verschmutzungsgrad nach IEC 61010_1",
        "Relative Luftfeuchtigkeit dauerhaft _%_",
        "Relative Luftfeuchtigkeit kurzzeitig _%_",
        "Verpackungseinheit _VPE_",
        "VPE auf Palette",
        "Verpackungsabmessungen Breite _mm_",
        "Verpackungsabmessungen Höhe _mm_",
        "Verpackungsabmessungen Tiefe _mm_",
    ];

    const FIELD_VALUE_EMPTY = [
        'n/a',
        'x'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'availability',
        'vendor',
        'product_family',
        'article_number',
        'short_description',
        'long_description',
        'ean',
        'slug',
        'price',
        'fee_number',
        'active',
        'product_type_lvl4',
        'product_type_lvl5',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'categories',
        'created_at',
        'updated_at',
        'product_images',
        'principle_images',
        'assembly_instructions',
        'dimensional_images'
    ];

    protected $appends = [
        'attributes',
        'category_ids',
        'product_images',
        'principle_images',
        'dimensional_images',
        'assembly_instructions',
        'instruction_url',
        'dimensional_url',
        'images',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'online' => 'boolean',
        'ean' => 'integer',
        'fee_number' => 'integer',
        'width' => 'integer',
        'height' => 'integer',
        'depth' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories() {
        return $this->belongsToMany(Category::class, 'article_categories');
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $categories = [];
        foreach ($this->categories as $category) {
            $categories[] = $category->title;
        }
        return [
            'id' => $this->id,
            'title' => $this->title,
            'article_numberNgrams' => utf8_encode((new TNTIndexer)->buildTrigrams($this->article_number)),
            'short_description' => $this->short_description,
            'article_number' => $this->article_number,
            'category' => implode(' ', $categories),
            'ean' => $this->ean,
            'keywords' => $this->keywords,
        ];
    }

    public function getCategoryIdsAttribute () {
        return $this->categories->pluck('id');
    }

    /**
     * get the attributes of article
     */
    public function getAttributesAttribute()
    {
        $results = [];

        $attributes = ArticleAttribute::with([
            'attribute',
            'value'
        ])->where([
            ['article_id', '=', $this->id]
        ])->get();

       
        foreach ($attributes as $attribute) {
            dd($attribute);
            $results[] = [
                "id" => $attribute->attribute->id,
                "label" => $attribute->attribute->title,
                "group" => $attribute->attribute->group,
                "value" => $attribute->value->value,
            ];
        }

        return $results;
    }

    protected function getImages($field) {
        $images = [];

        $files = FileReference::select([
            'files.id as id',
            'files.name as name',
            'file_references.field_name as field',
            'file_references.sorting as sorting',
            'files.identifier as url'
        ])->join('files', 'file_references.id_local', '=', 'files.id')
            ->where([
                'tablenames' => 'articles',
                'field_name' => $field,
                'id_foreign' => $this->id,
            ])->orderBy('sorting', 'asc')->get()->toArray();


        foreach ($files as $file) {
            $image = $file;
            $image['url'] = request()->getSchemeAndHttpHost() . '/img/'. $file['url'];
            $images[] = $image;
        }

        return $images;
    }

    public function getProductImagesAttribute () {
        return $this->getImages('produktbilder');
    }

    public function getPrincipleImagesAttribute () {
        return $this->getImages('prinzipdarstellung');
    }

    public function getDimensionalImagesAttribute () {
        return $this->getImages('maßdarstellung');
    }
    public function getInstructionUrlAttribute () {
        if (count($this->assembly_instructions) > 0) {
            return request()->getSchemeAndHttpHost() . '/instructions/'. $this->article_number;
        }
        return false;
    }

    public function getDimensionalUrlAttribute () {
        if (count($this->dimensional_images) > 0) {
            return request()->getSchemeAndHttpHost() . '/dimensions/'. $this->article_number;
        }
        return false;
    }

    public function getAssemblyInstructionsAttribute () {
        return FileReference::select([
            'files.id as id',
            'files.name as name',
            'file_references.field_name as field',
            'file_references.sorting as sorting',
            'files.identifier as url'
        ])->join('files', 'file_references.id_local', '=', 'files.id')
            ->where([
                'tablenames' => 'articles',
                'field_name' => 'montageanleitung',
                'id_foreign' => $this->id,
            ])->orderBy('sorting', 'asc')->get()->toArray();
    }

    public function getImagesAttribute () {
        return array_merge($this->product_images, $this->principle_images, $this->dimensional_images);;
    }
}
