<?php

namespace App\Console\Commands;

use App\Jobs\ImportArticlesJob;
use App\Models\Category;
use App\Services\ImportService;
use Illuminate\Console\Command;


class ImportProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wem:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports products from SE';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        ImportService::truncateArticles();
        Category::truncate();

        ImportArticlesJob::dispatch('Kleinverteiler', 0, 2000, true);
        ImportArticlesJob::dispatch('Feldverteiler', 0, 2000, true);
        ImportArticlesJob::dispatch('ZP', 0, 2000, true);
    }
}
