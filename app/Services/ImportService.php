<?php

namespace App\Services;

use App\Models\Article;
use App\Models\ArticleAttribute;
use App\Models\ArticleCategory;
use App\Models\Attribute;
use App\Models\AttributeGroup;
use App\Models\AttributeValue;
use App\Models\Category;
use App\Models\File;
use App\Models\FileReference;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ImportService
{

    public static function truncateArticles () {
        Article::truncate();
        ArticleAttribute::truncate();
        ArticleCategory::truncate();
        FileReference::truncate();
    }

    public static function getArticleList($offset, $limit, $layout = 'web1-Artikel_DE')
    {
        $url = FileMakerService::getBaseUrl($layout) . '/_find';
        $results = [];

        $query = [
            "query" => [],
            "limit" => $limit
        ];

        $query["query"][] = [
            "Artikelnummer" => "*"
        ];

        if ($offset > 0) {
            $query["offset"] = $offset;
        }

        try {
            $content = FileMakerService::post($url, $query);
            if ($content) {
                $parsedContent = $content;
                return $parsedContent->response->data;
            }
        } catch (\Exception $ex) {
            throw $ex;
        }

        return $results;
    }


    public static function findImage($name)
    {
        $url = FileMakerService::getBaseUrl('Bilder') . '/_find';

        $results = [];

        $query = [
            "query" => [],
            "limit" => 10
        ];

        $query["query"][] = [
            "Bilder::Dateiname" => $name
        ];

        try {
            $content = FileMakerService::post($url, $query);
            if ($content) {
                $parsedContent = $content;
                return $parsedContent->response->data;
            }
        } catch (\Exception $ex) {

        }

        return $results;
    }

    public static function importAttributeFromCsvFile($file, $truncateBefore = true)
    {
        if ($truncateBefore) { Attribute::truncate(); }

        $row = 0;

        if (($handle = fopen($file, "r")) !== FALSE) {

            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $row++;

                $id = (int)$data[1];

                // Produktart, gem. PM0 - Level 4, Produktart, gem. PM0 - Level 5
                if ($row === 1 || $id < 2000 || $id > 100000) {
                    continue;
                }

                $attributeGroup = AttributeGroup::updateOrCreate(
                    [
                        'title' => $data[0],
                    ],
                    [
                        'title' => $data[0],
                        'sorting' => $row,
                    ]
                );

                Attribute::updateOrCreate(
                    [
                        'id' => (int)$data[1]
                    ],
                    [
                        'title' => $data[2],
                        'attribute_group_id' => $attributeGroup->id,
                        'slug' => Str::slug($data[2]),
                        'standard_filter' => in_array((int) $id, [2010, 2009, 1103, 3016])
                    ]
                );

            }
        }
    }

    public static function importFromXmlFile($file, $truncateBefore = true)
    {
        if ($truncateBefore) {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            Article::truncate();
            ArticleCategory::truncate();
            FileReference::where([
                ['tablenames', '=', 'articles']
            ])->delete();
            ArticleAttribute::truncate();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        }
        $fileContent = file_get_contents($file);

        $xml = simplexml_load_string($fileContent);
        $array = self::XML2Array($xml);
       //  $array = array($xml->getName() => $array);

        $worksheets = $array['children']['worksheet'];

        foreach ($worksheets as $key => $worksheet) {
            $worksheetName = $worksheet['attributes']['ss:name'];

            if ($worksheetName === 'DB_Aufbau') {

            }
            else {
                $datas = self::getProductsFromExcelWorksheetArray($worksheet);

                foreach ($datas as $item) {
                    $mainCategory = null;
                    $secondCategory = null;

                    foreach ($item as $attribute) {

                        $attributeId = (int) $attribute['id'];

                        switch ($attributeId) {
                            case 1003: $modelData['vendor'] = $attribute['value']; break; // vendor
                            case 1001:
                                $modelData['article_number'] = $attribute['value'];
                                $modelData['slug'] = Str::slug($attribute['value']);
                                break; // article no
                            case 1007: $modelData['title'] = $attribute['value']; break; // title
                            case 1008: $modelData['short_description'] = $attribute['value']; break; // short_description
                            case 1011: $modelData['long_description'] = $attribute['value']; break; // long_description
                            case 1009: $modelData['ean'] = $attribute['value']; break; // ean
                        }

                        if (in_array($attributeId, [1004, 1005])) {
                            if ($attributeId === 1004) {
                                $mainCategoryName = $attribute['value'];
                                $mainCategory = Category::updateOrCreate(
                                    [
                                        'title' => $mainCategoryName,
                                        'parent_id' => 0
                                    ],
                                    [
                                        'title' => $mainCategoryName,
                                        'parent_id' => 0,
                                        'slug' => \Str::slug($mainCategoryName)
                                    ]
                                );
                            }
                            if ($attributeId === 1005) {
                                $secondCategoryName = $attribute['value'];
                                $secondCategory = Category::updateOrCreate(
                                    [
                                        'title' => $secondCategoryName,
                                        'parent_id' => $mainCategory->id
                                    ],
                                    [
                                        'title' => $secondCategoryName,
                                        'parent_id' => $mainCategory->id,
                                        'slug' => \Str::slug($mainCategoryName) .  '-' . \Str::slug($secondCategoryName)
                                    ]
                                );
                            }
                        }
                    }

                    $article = Article::updateOrCreate(
                        [
                            'article_number' => $modelData['article_number']
                        ],
                        $modelData
                    );

                    foreach ($item as $attribute) {
                        $attributeId = (int) $attribute['id'];
                        if ($attributeId >= 8000000) {
                            $value = $attribute['value'];
                            if (strtolower($value) !== 'x') {
                                $file = File::where([
                                    ['name', '=', strtolower($value)]
                                ])->first();
                                if ($file) {

                                    if ($attributeId > 8000000 && $attributeId <= 8000007) {
                                        $fieldName = 'article_images';
                                    } elseif ($attributeId > 8000007 && $attributeId <= 8000011) {
                                        $fieldName = 'article_prinzip';
                                    } elseif ($attributeId > 8000011 && $attributeId <= 8000012) {
                                        $fieldName = 'article_mass';
                                    }

                                    FileReference::updateOrCreate([
                                        'id_foreign' => $article->id,
                                        'field_name' => $fieldName,
                                        'tablenames' => 'articles',
                                        'sorting' => $attributeId - 8000000,
                                    ], [
                                        'field_name' => $fieldName,
                                        'id_local' => $file->id
                                    ]);
                                }
                            }

                        }
                    }

                    if (!is_null($mainCategory)) {
                        ArticleCategory::updateOrCreate([
                            'article_id' => $article->id,
                            'category_id' => $mainCategory->id,
                        ], [
                            'article_id' => $article->id,
                            'category_id' => $mainCategory->id,
                        ]);
                    }
                    if (!is_null($secondCategory)) {
                        ArticleCategory::updateOrCreate([
                            'article_id' => $article->id,
                            'category_id' => $secondCategory->id,
                        ], [
                            'article_id' => $article->id,
                            'category_id' => $secondCategory->id,
                        ]);
                    }

                    ArticleAttribute::where([
                        ['article_id', '=', $article->id]
                    ])->delete();

                    foreach ($item as $attribute) {

                        $attributeId = (int) $attribute['id'];

                        if ($attributeId >= 2000 && $attributeId < 8000 && $attributeId !== 3016) {

                            $value = $attribute['value'];

                            if ($value === 'N/A') {
                                continue;
                            }

                            $attributeValue = AttributeValue::updateOrCreate(
                                [
                                    'attribute_id' => $attributeId,
                                    'value' => $value
                                ],
                                [
                                    'attribute_id' => $attributeId,
                                    'value' => $value
                                ]
                            );

                            ArticleAttribute::create([
                                'article_id' => $article->id,
                                'attribute_id' => $attributeId,
                                'attribute_value_id' => $attributeValue->id
                            ]);
                        }
                    }

                }
            }
        }
    }

    public static function getProductsFromExcelWorksheetArray ($worksheet) {
        $rows = $worksheet['children']['table'][0]['children']['row'];
        $header = [];
        $datas = [];
        $headerCount = 0;

        foreach ($rows as $key => $row) {
            $cells = $row['children']['cell'];

            if ($key === 0) {
                foreach ($cells as $cellKey => $cell) {
                    $header[$cellKey] = [
                        'index' => $cellKey,
                        'label' => $cell['children']['data'][0]['text']
                    ];
                    $headerCount++;
                }
            } elseif ($key === 1) {

            } elseif ($key === 2) {
                foreach ($cells as $cellKey => $cell) {
                    $header[$cellKey]['id'] = $cell['children']['data'][0]['text'];
                }
            } else {
               $index = 0;
               $data = $header;

                if(count($cells) !== $headerCount) {
                    continue;
                }

                foreach ($cells as $cellKey => $cell) {

                    if (key_exists('ss:index', $cell['attributes'])) {
                        $index = (int) $cell['attributes']['ss:index'] - 1;
                    }

                    $data[$index]['value'] = $cell['children']['data'][0]['text'];
                    $data[$index]['type'] = $cell['children']['data'][0]['attributes']['ss:type'];
                    $index++;
                }

                $datas[] = $data;

            }
        }

        return $datas;
    }

    public static function XML2Array(\SimpleXMLElement $obj)
    {
        $namespace = $obj->getDocNamespaces(true);
        $namespace[NULL] = NULL;

        $children = array();
        $attributes = array();
        $name = strtolower((string)$obj->getName());

        $text = trim((string)$obj);
        if (strlen($text) <= 0) {
            $text = NULL;
        }

        // get info for all namespaces
        if (is_object($obj)) {
            foreach ($namespace as $ns => $nsUrl) {
                // atributes
                $objAttributes = $obj->attributes($ns, true);
                foreach ($objAttributes as $attributeName => $attributeValue) {
                    $attribName = strtolower(trim((string)$attributeName));
                    $attribVal = trim((string)$attributeValue);
                    if (!empty($ns)) {
                        $attribName = $ns . ':' . $attribName;
                    }
                    $attributes[$attribName] = $attribVal;
                }

                // children
                $objChildren = $obj->children($ns, true);
                foreach ($objChildren as $childName => $child) {
                    $childName = strtolower((string)$childName);
                    if (!empty($ns)) {
                        $childName = $ns . ':' . $childName;
                    }
                    $children[$childName][] = self::XML2Array($child);
                }
            }
        }

        return array(
            'name' => $name,
            'text' => $text,
            'attributes' => $attributes,
            'children' => $children
        );

    }
}
