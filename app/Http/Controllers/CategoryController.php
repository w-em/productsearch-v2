<?php

namespace App\Http\Controllers;

use App\Models\Category;

class CategoryController extends AbstractModelController
{
    protected $modelName = Category::class;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show', 'tree']]);
    }

    /**
     * Retrieves a tree-like structure of all categories.
     *
     * This method fetches all categories from the database, constructs a tree-like
     * data structure where each category can have child categories, and returns
     * the resulting data structure.
     *
     * @return \Illuminate\Http\JsonResponse A JSON response containing the category tree.
     */
    public function tree()
    {
        $categories = Category::all()->toArray();
        $data = [];
        $itemsByReference = array();

        //iterate on results row and create new index array of data
        foreach ($categories as $category) {
            $data[] = $category;
        }

        // Build array of item references:
        foreach ($data as $key => &$item) {
            $itemsByReference[$item['id']] = &$item;
        }

        // Set items as children of the relevant parent item.
        foreach ($data as $key => &$item) {
            if ($item['parent_id'] && isset($itemsByReference[$item['parent_id']])) {
                $itemsByReference [$item['parent_id']]['children'][] = &$item;
            }
        }

        // Remove items that were added to parents elsewhere:
        foreach ($data as $key => &$item) {
            if ($item['parent_id'] && isset($itemsByReference[$item['parent_id']]))
                unset($data[$key]);
        }

        return $this->sendResponseOk([
            "items" => $data
        ]);
    }

}
