<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Jobs\ImportArticlesJob;
use App\Jobs\ImportArticleJob;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\File;
use App\Models\FileDirectory;
use App\Models\User;
use App\Services\FileMakerService;
use App\Services\ImportService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

<<<<<<< HEAD
    public function import() {
=======
    protected function import() {
>>>>>>> remotes/origin/master

        ImportService::truncateArticles();
        Category::truncate();

        //$results = ImportService::getArticleList(0, 10, 'Kleinverteiler');
        //dd($results);

<<<<<<< HEAD
        ImportArticlesJob::dispatch('Kleinverteiler', 0, 2000, true);
        ImportArticlesJob::dispatch('Feldverteiler', 0, 2000, true);
        ImportArticlesJob::dispatch('ZP', 0, 2000, true);
=======
        ImportArticlesJob::dispatch('Kleinverteiler', 0, 1000, true);
        ImportArticlesJob::dispatch('Feldverteiler', 0, 1000, true);
        ImportArticlesJob::dispatch('ZP', 0, 1000, true);
>>>>>>> remotes/origin/master


        // ImportArticlesJob::dispatch('Leergehäuse', 0, 1000, false);
        // ImportArticlesJob::dispatch('ÜSS', 0, 1000, false);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        //dd(FileMakerService::createSession());
// $this->testCategory();
         $this->import();
        //$this->testCategory();
    }

    public function testCategory() {
        $results = ImportService::getArticleList(0, 1000, 'ZP');
        foreach ($results as $result) {
            if ($result->fieldData->Artikelnummer === "ABNPV107C02") {
                ImportArticleJob::dispatchSync($result);

                dd($result);
            }
        }
    }

    protected function getMainAttributesFromAttributeCsv() {

        $row = 0;
        $file = database_path('seeders/attributes.csv');
        if (($handle = fopen($file, "r")) !== FALSE) {

            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $row++;

                $id = (int)$data[1];

                // Produktart, gem. PM0 - Level 4, Produktart, gem. PM0 - Level 5
                if ($row === 1 || $id < 2000 || $id > 100000) {
                    continue;
                }

                $key = $data[2];
                $key = str_replace(['[', ']', '(', ')', '-', '/'], '_', $key);
                echo '"' . $key . '" => "' . $data[1] . '",' . "\n";
            }
        }
    }
}
