<?php

namespace App\Http\Controllers;

use App\Models\File;
use Illuminate\Http\Request;

class FileController extends AbstractModelController
{
    protected $modelName = File::class;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index']]);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = (int)$request->get('itemsPerPage', 50);
        $page = (int)$request->get('page', 1);
        $sortBy = $request->get('sortBy', []);
        $sortDesc = $request->get('sortDesc', []);
        $term = trim($request->get('term', ''));
        $parentId = (int) trim($request->get('parentId', 0));

        if (!empty($term)) {
            $query = $this->modelName::search($term);
        } else {
            $query = new $this->modelName;
        }

        foreach ($sortBy as $key => $value) {
            $query = $query->orderBy($value, (bool) $sortDesc[$key] === true ? 'desc' : 'asc');
        }

        $query = $query->where([
            ['file_directory_id', '=', $parentId]
        ]);

        $paginator = self::paginate($query->get(), $limit, $page);

        return $this->sendResponseOk([
            'items' => array_values($paginator->items()),
            'total' => $paginator->total(),
            'limit' => $paginator->perPage()
        ]);
    }

}
