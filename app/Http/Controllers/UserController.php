<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class UserController extends AbstractModelController
{
    protected $modelName = User::class;

   
    /**
     * Authenticates the user using the API middleware, except for the listed routes.
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => [ ]]);
    }

    /**
         * Creates a new user record in the database based on the provided request parameters.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\JsonResponse
         */
    public function create(Request $request) {
        $validated = $request->validate($this->modelName::RULES);

        $params = $request->all();
       
        $item = $this->modelName::create($params);

        return $this->sendResponseUpdated($item);

    }
}
