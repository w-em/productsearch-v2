<?php

namespace App\Http\Controllers;

use App\Models\FileDirectory;

class DirectoryController extends AbstractModelController
{
    protected $modelName = FileDirectory::class;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index']]);
    }

}
