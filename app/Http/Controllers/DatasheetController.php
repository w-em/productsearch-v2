<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DatasheetController extends Controller
{
    const FOOTER_LOGO_IMAGE = 'images/Brand_Logo_RGB_LifeGreen.png';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

  
    /**
     * Generates a PDF document containing a datasheet for a single article.
     *
     * @param Request $request The incoming HTTP request.
     * @param string $slug The slug of the article.
     * @return \Illuminate\Http\Response The generated PDF document.
     * @throws \Exception
     */
    protected function getById(Request $request, $slug)
    {
        $download = false;

        $article = Article::where([['slug', '=', $slug]])->first();
        if (!$article) {
            throw new NotFoundHttpException();
        }

        $this->trackDownload($slug, $request);

        $images = [];

        if (count($article->images) > 0) {
            foreach ($article->images as $image) {
                $images[] = [
                    "path" => storage_path('app/public/images/' . $image['field'] . '/' . $image['name']),
                    "type" => $image['field']
                ];
            }
        }
        $data = [
            'fontPath' => asset('storage/fonts/roboto-condensed-v19-latin/roboto-condensed-v19-latin-300.ttf'),
            'article' => $article,
            'images' => $images,
            'inlineCss' => file_get_contents(public_path('pdf/pdf.css')),
            'logoImage' => public_path(self::FOOTER_LOGO_IMAGE)
        ];

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('download.datasheet', $data);
        if ($download === true) {
            return $pdf->download($slug . '.pdf');
        } else {
            return $pdf->stream();
        }
    }

    
    /**
     * Generates a PDF document containing datasheets for multiple articles.
     *
     * @param Request $request The incoming HTTP request.
     * @param string $slugs A comma-separated list of article slugs.
     * @return \Illuminate\Http\Response The generated PDF document.
     */
    protected function getByIds(Request $request, $slugs)
    {
        $articles = [];

        $slugsArr = explode(',', $slugs);

        foreach ($slugsArr as $slug) {
            $article = Article::where([['slug', '=', $slug]])->first();
            if (!$article) {
                continue;
            }

            $this->trackDownload($slug, $request);

            $images = [];

            if (count($article->images) > 0) {
                foreach ($article->images as $image) {
                    $images[] = [
                        "path" => storage_path('app/public/images/' . $image['field'] . '/' . $image['name']),
                        "type" => $image['field']
                    ];
                }
            }

            $articles[] = [
                'article' => $article,
                'images' => $images
            ];
        }

        $data = [
            'fontPath' => asset('storage/fonts/roboto-condensed-v19-latin/roboto-condensed-v19-latin-300.ttf'),
            'articles' => $articles,
            'inlineCss' => file_get_contents(public_path('pdf/pdf.css')),
            'logoImage' => public_path(self::FOOTER_LOGO_IMAGE)
        ];

        // dd(storage_path('fonts/roboto-condensed-normal_05f82b1d798690119a8263c1eae2bd7e.ttf'));
        // return view('download.datasheets', $data)->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('download.datasheets', $data);

        return $pdf->stream();
    }

    protected function dimensions(Request $request, $slug) {

        $download = false;

        $article = Article::where([['slug', '=', $slug]])->first();
        if (!$article) {
            throw new NotFoundHttpException();
        }

        $images = [];

        if (count($article->dimensional_images) > 0) {
            foreach ($article->dimensional_images as $image) {
                $images[] = [
                    "path" => storage_path('app/public/images/' . $image['field'] . '/' . $image['name']),
                    "type" => $image['field']
                ];
            }
        }
        $data = [
            'fontPath' => asset('storage/fonts/roboto-condensed-v19-latin/roboto-condensed-v19-latin-300.ttf'),
            'article' => $article,
            'images' => $images,
            'title' => count($images) > 1 ? 'Maßzeichnungen' : 'Maßzeichnung',
            'inlineCss' => file_get_contents(public_path('pdf/pdf.css')),
            'logoImage' => public_path(self::FOOTER_LOGO_IMAGE)
        ];

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('download.dimensions', $data);
        if ($download === true) {
            return $pdf->download($slug . '.pdf');
        } else {
            return $pdf->stream();
        }
    }
}
