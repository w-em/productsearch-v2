<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

/**
 * Defines the interface for a model controller in the application.
 * This interface provides methods for handling common CRUD operations
 * on a model, as well as handling pagination of the model's index.
 */
interface ModelControllerInterface
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request);

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, int $id);

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id);

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, int $id);

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request);

    /**
     * @param Collection $items
     * @return Collection
     */
    public function beforeIndexPagination(Collection $items);
}
