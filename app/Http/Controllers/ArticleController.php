<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ArticleAttribute;
use App\Models\ArticleCategory;
use App\Models\AttributeValue;
use App\Services\ArticleSearch;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

class ArticleController extends AbstractModelController
{
    protected $modelName = Article::class;

    
    /**
     * Middleware to authenticate API requests, except for the specified routes.
     *
     * This middleware is applied to the ArticleController class, and it ensures that all
     * routes within the class are protected by authentication, except for the following:
     * - index
     * - show
     * - filters
     * - showBySlug
     * - export
     * - showBySlugs
     * - search
     * - filtersByToken
     *
     * This allows unauthenticated users to access the specified routes, while requiring
     * authentication for all other routes in the ArticleController.
     */
    public function __construct()
    {
        $this->middleware('auth:api', [
            'except' => [
                'index',
                'show',
                'filters',
                'showBySlug',
                'export',
                'showBySlugs',
                'search',
                'filtersByToken'
            ]]);
    }


    /**
     * Updates an existing article with the provided request data.
     *
     * This method handles the update of an article, including updating the associated article categories and attributes.
     *
     * @param Request $request The incoming HTTP request containing the updated article data.
     * @param int $id The ID of the article to be updated.
     * @return \Illuminate\Http\Response The updated article data.
     * @throws ModelNotFoundException If the article with the given ID is not found.
     */
    public function update(Request $request, int $id) {

        try {
            parent::update($request, $id);

            $item = $this->modelName::findOrFail($id);

            $params = $request->all();

            // article categories
            ArticleCategory::where([
                ['article_id', '=', $id]
            ])->delete();

            $articleCategories = [];

            foreach ($params['category_ids'] as $categoryId) {
                $articleCategories[] = [
                    'article_id' => $id,
                    'category_id' => $categoryId,
                ];
            }

            if (count ($articleCategories) > 0) {
                ArticleCategory::insert($articleCategories);
            }

            // article attributes
            ArticleAttribute::where([['article_id', '=', $id]])->delete();
            $articleAttributes = [];
            foreach ($params['attributes'] as $attribute) {

                $attributeValue = AttributeValue::updateOrCreate(
                    [
                        'attribute_id' => (int) $attribute['id'],
                        'value' => $attribute['value']
                    ],
                    [
                        'attribute_id' => (int) $attribute['id'],
                        'value' => $attribute['value']
                    ]
                );

                $articleAttributes[] = [
                    'article_id' => $id,
                    'attribute_id' => $attribute['id'],
                    'attribute_value_id' => $attributeValue->id,
                ];
            }

            if (count ($articleAttributes) > 0) {
                ArticleAttribute::insert($articleAttributes);
            }

            return $this->sendResponseUpdated($item);
        } catch (ModelNotFoundException $exception) {
            return $this->sendResponseNotFound();
        }
    }

    /**
     * Searches for articles based on the provided search parameters and returns a paginated response.
     *
     * @param Request $request The incoming HTTP request, containing the search parameters.
     * @return \Illuminate\Http\Response The paginated search results.
     */
    public function search(Request $request) {
        $page = (int)$request->get('page', 1);
        $limit = (int)$request->get('itemsPerPage', 24);
        $term = $request->get('term', '');
        $queries = $request->get('queries', []);
        $sort = $request->get('sort', []);
        $sha1 = sha1(json_encode($request->all()));
        $seconds = 20;

        $articleSearch = Cache::remember('article-search-' . $sha1, $seconds, function () use($term, $queries) {
            $articleSearch = new ArticleSearch($term, $queries);
            $articleSearch->search();
            return $articleSearch;
        });


        $query = Article::whereIn('id', $articleSearch->getResult());
        // TODO: sortby
        // $searchQuery->orderBy($sortBy, 'asc');

        $paginator = self::paginate($query->get()->makeHidden(['attributes']), $limit, $page);

        return $this->sendResponseOk([
            'items' => array_values($paginator->items()),
            'total' => $paginator->total(),
            'limit' => $paginator->perPage(),
            'token' => $sha1
        ]);
    }

    
    /**
     * Retrieves the filters associated with the specified search token.
     *
     * @param Request $request The incoming HTTP request, containing the search token.
     * @return \Illuminate\Http\JsonResponse The filters associated with the search token.
     */
    public function filtersByToken(Request $request) {
        $token = $request->input('token', null);
        if (is_null($token)) {
            return $this->sendResponseNotFound('token invalid');
        }

        if (!Cache::has('article-search-' . $token)) {
            return $this->sendResponseNotFound('token invalid');
        }

        $articleSearch = Cache::get('article-search-' . $token);
        $filters = $articleSearch->getFilters();

        return $this->sendResponseOk([
            'items' => $filters, // array_values($paginator->items()),
            'total' => count($filters), // $paginator->total(),
            'limit' => count($filters) //(int)$paginator->perPage(),
        ]);
    }

    
    /**
     * Retrieves a collection of articles based on the provided slugs.
     *
     * @param Request $request
     * @param string $slugs A comma-separated list of article slugs.
     * @return \Illuminate\Http\JsonResponse
     */
    public function showBySlugs(Request $request, string $slugs) {

        $slugs = explode(',', $slugs);

        $items = $this->modelName::whereIn('slug', $slugs)->get();

        return $this->sendResponseOk($items);
    }

}
