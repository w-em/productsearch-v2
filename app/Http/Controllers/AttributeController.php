<?php

namespace App\Http\Controllers;

use App\Models\Attribute;

class AttributeController extends AbstractModelController
{
    protected $modelName = Attribute::class;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

}
