<?php

namespace App\Http\Controllers;

use App\Models\AttributeGroup;

class AttributeGroupController extends AbstractModelController
{
    protected $modelName = AttributeGroup::class;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

}
