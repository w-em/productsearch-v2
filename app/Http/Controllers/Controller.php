<?php

namespace App\Http\Controllers;

use App\Models\DownloadTracking;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    /**
     * Track a download of an article.
     *
     * This method updates or creates a new DownloadTracking record for the given article number,
     * IP address, and browser user agent. This allows tracking downloads of articles.
     *
     * @param int $articleNumber The article number to track.
     * @param \Illuminate\Http\Request $request The current HTTP request.
     * @return void
     */
    public function trackDownload($articleNumber, Request $request) {
        DownloadTracking::updateOrCreate(
            [
                'article_number' => $articleNumber,
                'ip_address' => $request->ip(),
                'browser' => $request->header('User-Agent'),
            ],
            [
                'article_number' => $articleNumber,
            ]
        );
    }

    
    /**
     * Send a response with the given data, message, and status code.
     *
     * @param mixed $data The data to include in the response.
     * @param string $message The message to include in the response.
     * @param int $statusCode The HTTP status code to use for the response.
     * @param array $headers Any additional headers to include in the response.
     * @return \Illuminate\Http\Response The response object.
     */
    public function sendResponse($data = null, $message = '', $statusCode = 200,$headers = [])
    {
        $d = [
            'message' => $message,
            'data' => $data
        ];

        return response( $d, $statusCode, $headers);
    }

    
    /**
     * Send an OK response with the given data, message, and headers.
     *
     * @param mixed $data The data to include in the response.
     * @param string $message The message to include in the response.
     * @param array $headers Any additional headers to include in the response.
     * @return \Illuminate\Http\Response The response object.
     */
    public function sendResponseOk($data = [],string $message = "Resource found.", array $headers = [])
    {
        return $this->sendResponse($data, $message,200, $headers);
    }

    
    public function sendResponseNotFound(string $message = "Resource not found.", array $headers = [])
    {
        return $this->sendResponse([],$message,404,$headers);
    }

    
    public function sendResponseBadRequest(string $message = "Bad Request.", array $headers = [])
    {
        return $this->sendResponse([],$message,400,$headers);
    }

    /**
     * send created response
     *
     * @param string $message
     * @param array $data
     * @param array $headers
     * @return \Illuminate\Http\Response
     */
    public function sendResponseCreated($data = [], string $message = "Resource created.", array $headers = [])
    {
        return $this->sendResponse($data,$message,201,$headers);
    }

    /**
     * send updated response
     *
     * @param string $message
     * @param array $data
     * @param array $headers
     * @return \Illuminate\Http\Response
     */
    public function sendResponseUpdated($data = [],string $message = "Resource updated.", array $headers = [])
    {
        return $this->sendResponse($data,$message,200,$headers);
    }

    /**
     * send deleted response
     *
     * @param string $message
     * @param array $headers
     * @return \Illuminate\Http\Response
     */
    public function sendResponseDeleted(string $message = "Resource deleted.",array $headers = [])
    {
        return $this->sendResponse([],$message,200,$headers);
    }

    /**
     * send forbidden response
     *
     * @param string $message
     * @param array $headers
     * @return \Illuminate\Http\Response
     */
    public function sendResponseForbidden(string $message = "Action forbidden.",array $headers = [])
    {
        return $this->sendResponse([],$message,403,$headers);
    }

    /**
     * send no content
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponseNoContent()
    {
        return response(null,204);
    }
}
