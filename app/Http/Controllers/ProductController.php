<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class ProductController extends Controller
{

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = (int)$request->get('itemsPerPage', 24);
        $page = (int)$request->get('page', 1);
        $sortBy = $request->get('sortBy', []);
        $sortDesc = $request->get('sortDesc', []);
        $term = trim($request->get('term', ''));

        $query = new Product;

        foreach ($sortBy as $key => $value) {
            $query->orderBy($value, (bool) $sortDesc[$key] === true ? 'desc' : 'asc');
        }

        if (strlen($term) > 0) {
            $query = $query->where([['name', 'LIKE', '%' . $term . '%']]);
        }

        $paginator = self::paginate($query->get(), $limit, $page);

        return $this->sendResponseOk([
            'items' => array_values($paginator->items()),
            'total' => $paginator->total(),
            'limit' => $paginator->perPage()
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, int $id) {

        $item = Product::findOrFail($id);

        return $this->sendResponseOk([
            'item' => $item
        ]);
    }

    /**
     * creates a pagination of collection of items
     *
     * @param array|Collection $items
     * @param int $perPage
     * @param int $page
     * @param array $options
     *
     * @return LengthAwarePaginator
     */
    public static function paginate($items, $perPage = 12, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
