<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\File;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use League\Glide\Responses\LaravelResponseFactory;
use League\Glide\ServerFactory;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ImageController extends Controller
{
    protected $modelName = File::class;

    /**
     * @return \Illuminate\Http\Response
     */
    public function index(Filesystem $filesystem, $path)
    {
        $server = ServerFactory::create([
            'response' => new LaravelResponseFactory(app('request')),
            'source' => $filesystem->getDriver(),
            'cache' => $filesystem->getDriver(),
            'cache_path_prefix' => '.cache',
            'base_url' => 'img',
            'driver' => 'imagick'
        ]);

        $path = '/public/images/' . $path;


        return $server->getImageResponse($path, request()->all());
    }

    public function instructions(Request $request, $slug) {

        $item = Article::where([
            ['slug', '=', $slug]
        ])->first();

        if (!$item) {
            throw new ModelNotFoundException('Article not found');
        }

        $files = [];
        foreach($item->assembly_instructions as $filename) {
            $file = storage_path('app/public/images/' . $filename['url']);
            if (file_exists($file)) {
                $files[] = $file;
            }
        }
        if (count($files) === 0) {
            abort(404);
        }

        try {
            $pdf = new \Clegginabox\PDFMerger\PDFMerger;
            foreach ($files as $file) {
                $pdf->addPDF($file, 'all');
            }
            return $pdf->merge('browser');
        } catch (\Exception $exception) {
            $filename = $item->name;
            $path = $files[0];

            return response()->file($path, [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="'.$filename.'"'
            ]);
        }
    }
}
