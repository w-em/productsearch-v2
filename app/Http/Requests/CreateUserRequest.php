<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest {
    public function authorized () {
        return true;
    }

    public function rules () {
        return [
            'email' => [
                'required',
                'email',
                'unique:App\Models\User,email',
            ],

            'password' => [
                'required',
                'min:8'
            ]
        ];
    }
}
