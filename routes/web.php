<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$frontendDomain = \Illuminate\Support\Facades\Config::get('app.app_url');
$backendDomain = \Illuminate\Support\Facades\Config::get('app.backend_url');

Route::get('/test', [App\Http\Controllers\TestController::class, 'index'])->where('path', '.*');
Route::get('/import-all', [App\Http\Controllers\TestController::class, 'index'])->where('path', '.*');
Route::get('/instructions/{slug}', [App\Http\Controllers\ImageController::class, 'instructions'])->where('slug', '.*');
Route::get('/dimensions/{slug}', [App\Http\Controllers\DatasheetController::class, 'dimensions'])->where('slug', '.*');
Route::get('/datasheets/{slugs?}', [App\Http\Controllers\DatasheetController::class, 'getByIds'])->where('ids', '[\/\w\.-]*');
Route::get('/datasheet/{slug?}', [App\Http\Controllers\DatasheetController::class, 'getById'])->where('slug', '[\/\w\.-]*');
Route::get('/img/{path}', [App\Http\Controllers\ImageController::class, 'index'])->where('path', '.*');

Route::domain($backendDomain)->get('/{path}', function () {
    return view('backend');
})->where('path', '(.*)');


Route::get('/{path}', function () {
    return view('frontend');
})->where('path', '(.*)');



// Auth::routes();

// Route::get('/test', [App\Http\Controllers\TestController::class, 'index'])->name('test');
