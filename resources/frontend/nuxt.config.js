import path from 'path'
// import PurgecssPlugin from 'purgecss-webpack-plugin'
import glob from 'glob-all'
const { join } = require('path')
const { copySync, removeSync } = require('fs-extra')

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,
  // Target: https://go.nuxtjs.dev/config-target
  // target: 'static',
  srcDir: '',

  env: {
    baseUrl: process.env.BASE_URL || 'https://abn-products.w-em.com/',
    apiUrl: process.env.API_URL || 'https://abn-products.w-em.com/api',
    appName: process.env.APP_NAME || 'ABN-Electro products',
    appLocale: process.env.APP_LOCALE || 'en'
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - ABN-Electro Produktdatenbank',
    title: 'ABN-Electro Produktdatenbank', 
    htmlAttrs: {
      lang: 'de'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'theme-color', content: '#1FA22E', media: '(prefers-color-scheme: light)' },
      { name: 'theme-color', content: '#1FA22E', media: '(prefers-color-scheme: dark)' },
      { hid: 'description', name: 'description', content: 'ABN-Electro Produktdatenbank' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'shortcut icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  loading: {
    color: '#42b4e6',
    height: '5px'
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~plugins/i18n.js',
    '~plugins/axios.js',
    { src: '~plugins/vuex-persist.js' },
    { src: '~plugins/swiper', mode: 'client' },
    '~plugins/api/attributeApiService.js',
    '~plugins/api/categoryApiService.js',
    '~plugins/api/articleApiService.js'
  ],

  router: {
    middleware: ['locale']
  },

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    // '@nuxt/postcss8',
    // 'nuxt-purgecss',
    '@nuxtjs/google-fonts'
  ],
  googleFonts: {
    families: {
      Roboto: {
        wght: [100, 300, 400, 500, 700],
        ital: [100, 300, 400, 500, 700]
      }
    },
    display: 'swap',
    prefetch: true,
    preconnect: true,
    preload: true,
    download: true,
    base64: false
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    'nuxtjs-mdi-font',
    '@nuxtjs/pwa'
    /*
    [
      'nuxt-compress',
      {
        gzip: {
          cache: true
        },
        brotli: false
      }
    ] */
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    proxy: true,
    credentials: true
  },
  proxy: {
    '/api': {
      target: 'https://productsearch-v2.ddev.site',
      pathRewrite: { '^/api': '/api' },
      secure: false
    },
    '/datasheet': {
      target: 'https://productsearch-v2.ddev.site',
      pathRewrite: { '^/datasheet': '/datasheet' },
      secure: false
    },
    '/datasheets': {
      target: 'https://productsearch-v2.ddev.site',
      pathRewrite: { '^/datasheets': '/datasheets' },
      secure: false
    },
    '/img': {
      target: 'https://productsearch-v2.ddev.site',
      pathRewrite: { '^/img': '/img' },
      secure: false
    },
    '/instructions': {
      target: 'https://productsearch-v2.ddev.site',
      pathRewrite: { '^/instructions': '/instructions' },
      secure: false
    },
    '/dimensions': {
      target: 'https://productsearch-v2.ddev.site',
      pathRewrite: { '^/dimensions': '/dimensions' },
      secure: false
    }
  },
  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    defaultAssets: {
      // icons: false
      icons: {
        iconfont: 'mdi'
      }
    },
    theme: {
      dark: false,
      themes: {
        light: {
          primary: '#3dcd58',
          secondary: '#42b4e6',
          background: '#f0f2f5'
        },
        dark: {
          primary: '#42b4e6',
          background: '#42b4e6'
        }
      }
    }
  },
  pwa: {
    meta: {
      name: 'ABN Produktdatenbank',
      author: 'Stefan Jaeger | sj@w-em.com',
      description: 'ABN Produktdatenbank',
      background_color: '#ffffff',
      nativeUI: false
    },
    manifest: {
      name: 'ABN Produktdatenbank',
      short_name: 'ABN Products',
      description: 'ABN Produktdatenbank',
      background_color: '#ffffff',
      theme_color: '#3dcd58',
      lang: 'de',
      useWebmanifestExtension: false
    }
  },
  build: {
    extractCSS: true,
    /*
    optimization: {
      splitChunks: {
        cacheGroups: {
          styles: {
            name: 'styles',
            test: /\.(css|vue)$/,
            chunks: 'all',
            enforce: true
          }
        }
      }
    },
    filenames: {
      app: ({ isDev, isModern }) => isDev ? `[name]${isModern ? '.modern' : ''}.js` : `js/[contenthash:7]${isModern ? '.modern' : ''}.js`,
      chunk: ({ isDev, isModern }) => isDev ? `[name]${isModern ? '.modern' : ''}.js` : `js/[contenthash:7]${isModern ? '.modern' : ''}.js`,
      css: ({ isDev }) => isDev ? '[name].css' : 'css/[contenthash:7].css',
      img: ({ isDev }) => isDev ? '[path][name].[ext]' : 'img/[name].[contenthash:7].[ext]',
      font: ({ isDev }) => isDev ? '[path][name].[ext]' : 'fonts/[name].[contenthash:7].[ext]',
      video: ({ isDev }) => isDev ? '[path][name].[ext]' : 'videos/[name].[contenthash:7].[ext]'
    },
    postcss1: {
      plugins: {
        'postcss-nesting': {},
        'postcss-normalize': {},
        'postcss-url': {},
        'postcss-object-fit-images': {}
      },
      preset: {
        preserve: false,
        stage: 0,
        features: {
          'custom-media-queries': false,
          'nesting-rules': false
        },
        autoprefixer: {
          grid: true,
          flexbox: true,
          overrideBrowserslist: ['last 3 versions', '> 1%', 'ie 11']
        }
      }
    }, */
    publicPath: '/_frontend/'
  },
  generate: {
    subFolders: false,
    dir: 'frontend-dist'
  },
  render: {
    csp: true,
    crossorigin: 'anonymous',
    resourceHints: true,
    http2: { push: true },
    static: {
      maxAge: 1000 * 60 * 60 * 24 * 156
    },
    bundleRenderer: {
      shouldPreload: (file, type) => {
        return ['script', 'font', 'style'].includes(type)
      },
      shouldPrefetch: (file, type) => {
        return ['style', 'font'].includes(type)
      }
    }
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  hooks: {
    build: {
      done (generator) {
        // Copy dist files to public/_nuxt
        if (generator.nuxt.options.dev === false && generator.nuxt.options.ssr === false) {
          const publicDir = join(generator.nuxt.options.rootDir, 'public', '_frontend')
          const viewsDir = join(generator.nuxt.options.rootDir, 'resources', 'views')
          removeSync(publicDir)
          copySync(join(generator.nuxt.options.rootDir, 'frontend-dist', '_frontend'), publicDir)
          copySync(join(generator.nuxt.options.rootDir, 'frontend-dist', '200.html'), join(viewsDir, 'frontend.blade.php'))
          removeSync(join(generator.nuxt.options.rootDir, 'frontend-dist'))
        }
      }
    }
  }
}
