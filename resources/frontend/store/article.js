import { set } from '@/utils/vuex'

export const state = () => ({
  loading: true,
  articles: [],
  categoryTree: [],
  selectedCategories: [],
  filters: [],
  categories: []
})

export const mutations = {
  setLoading: set('loading'),
  setArticles: set('articles'),
  setCategoryTree: set('categoryTree'),
  setSelectedCategories: set('selectedCategories'),
  setCategories: set('categories'),
  setFilters: set('filters')
}

// getters
export const getters = {
  loading: state => state.loading,
  articles: state => state.articles,
  categoryTree: state => state.categoryTree,
  selectedCategories: state => state.selectedCategories,
  categories: state => state.categories,
  filters: state => state.filters
}
