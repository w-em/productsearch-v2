export const state = () => ({
  items: []
})

export const mutations = {
  clear (state) {
    state.items = []
  },
  add (state, slug) {
    if (!state.items.find(x => x.slug === slug)) {
      state.items.push({
        slug,
        amount: 1
      })
    }
  },
  remove (state, slug) {
    const index = state.items.findIndex(x => x.slug === slug)
    if (index > -1) {
      state.items.splice(index, 1)
    }
  },
  changeAmount (state, payload) {
    const index = state.items.findIndex(x => x.slug === payload.slug)
    if (index > -1) {
      state.items.splice(index, 1, payload)
    }
  }
}

// getters
export const getters = {
  items: state => state.items
}
