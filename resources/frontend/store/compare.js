export const state = () => ({
  maxCompareItems: 4,
  items: []
})

export const mutations = {
  add (state, slug) {
    if (!state.items.find(x => x === slug)) {
      state.items.push(slug)
    }
  },
  remove (state, slug) {
    const index = state.items.findIndex(x => x === slug)
    if (index > -1) {
      state.items.splice(index, 1)
    }
  }
}

// getters
export const getters = {
  items: state => state.items,
  maxCompareItems: state => state.maxCompareItems
}
