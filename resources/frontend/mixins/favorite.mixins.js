export default {
  methods: {
    onFavoriteChange () {
      if (this.favoriteSelected) {
        this.$store.commit('favorite/remove', this.item.slug)
      } else {
        this.$store.commit('favorite/add', this.item.slug)
      }
    }
  },
  computed: {
    favoriteSelected: {
      get () {
        return this.$store.getters['favorite/items'].find(x => x.slug === this.item.slug)
      },
      set (val) {
      }
    }
  }
}
