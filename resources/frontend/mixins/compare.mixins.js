export default {
  methods: {
    onCompareChange (evt) {
      if (this.compareSelected) {
        this.$store.commit('compare/remove', this.item.slug)
      } else {
        this.$store.commit('compare/add', this.item.slug)
      }
    }
  },
  computed: {
    compareSelected: {
      get () {
        return this.$store.getters['compare/items'].find(x => x === this.item.slug)
      },
      set (val) {
      }
    },
    compareDisabled () {
      return this.$store.getters['compare/items'].length >= this.$store.getters['compare/maxCompareItems'] && !!this.compareSelected === false
    }
  }
}
