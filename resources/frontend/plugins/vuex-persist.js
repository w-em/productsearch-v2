// import * as Cookies from 'js-cookie'
// import cookie from 'cookie'

import { VuexPersistence } from 'application/resources/frontend/plugins/vuex-persist'

export default ({ store }) => {
  new VuexPersistence({
    /* your options */
    modules: ['compare', 'favorite'],
    key: 'abn-products', // The key to store the state on in the storage provider.
    storage: window.localStorage // or window.sessionStorage or localForage
  }).plugin(store)
}
