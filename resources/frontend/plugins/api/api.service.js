import parseJsonApi from './jsonapi-parser.service'
// import store from '@/store'
/**
 * ApiService class which provides the common methods for our REST API
 * @class
 */
class ApiService {
  /**
   * @constructor
   * @param {axios} httpClient
   * @param {String} apiEndpoint
   * @param {String} [contentType='application/vnd.api+json']
   */
  constructor (httpClient, apiEndpoint, contentType = 'application/vnd.api+json') {
    this.apiEndpoint = apiEndpoint
    this.httpClient = httpClient
    this.contentType = contentType
  }

  /**
   * Gets a list from the configured API end point using the page & limit.
   *
   * @param {Number} page
   * @param {Number} itemsPerPage
   * @param {Array} sortBy
   * @param {Array} sortDesc
   * @param {String} term
   * @param {Array} queries
   * @returns {Promise<T>}
   */
  getList ({
    page = 1,
    itemsPerPage = 50,
    sortBy,
    sortDesc,
    term,
    queries
  }) {
    const requestHeaders = this.getBasicHeaders({})
    const params = { page, itemsPerPage, sortBy, sortDesc }

    if (term) {
      params.term = term
    }

    if (term) {
      params.term = term
    }

    if (queries) {
      for (const key in queries) {
        params[key] = queries[key]
      }
    }

    return this.httpClient
      .get(this.getApiBasePath(), { params, headers: requestHeaders })
      .then((response) => {
        return ApiService.handleResponse(response)
      })
  }

  /**
   * Get the detail entity from the API end point using the provided entity id.
   *
   * @param {String} slug
   * @param {Object} additionalParams
   * @param {Object} additionalHeaders
   * @returns {Promise<T>}
   */
  getBySlug (slug, additionalParams = {}, additionalHeaders = {}) {
    if (!slug) {
      return Promise.reject(new Error('Missing required argument: slug'))
    }

    const params = additionalParams
    const headers = this.getBasicHeaders(additionalHeaders)

    return this.httpClient
      .get(this.getApiBasePath('slug/' + slug), {
        params,
        headers
      })
      .then((response) => {
        return ApiService.handleResponse(response)
      })
  }

  /**
   * Get the detail entity from the API end point using the provided entity id.
   *
   * @param {String|Number} id
   * @param {Object} additionalParams
   * @param {Object} additionalHeaders
   * @returns {Promise<T>}
   */
  getById (id, additionalParams = {}, additionalHeaders = {}) {
    if (!id) {
      return Promise.reject(new Error('Missing required argument: id'))
    }

    const params = additionalParams
    const headers = this.getBasicHeaders(additionalHeaders)

    return this.httpClient
      .get(this.getApiBasePath(id), {
        params,
        headers
      })
      .then((response) => {
        return ApiService.handleResponse(response)
      })
  }

  /**
   * Updates an entity using the provided payload.
   *
   * @param {String|Number} id
   * @param {Object} payload
   * @param {Object} additionalParams
   * @param {Object} additionalHeaders
   * @returns {Promise<T>}
   */
  updateById (id, payload, additionalParams = {}, additionalHeaders = {}) {
    if (!id) {
      return Promise.reject(new Error('Missing required argument: id'))
    }

    const params = additionalParams
    const headers = this.getBasicHeaders(additionalHeaders)

    return this.httpClient
      .put(this.getApiBasePath(id), payload, {
        params,
        headers
      })
      .then((response) => {
        return ApiService.handleResponse(response)
      })
  }

  /**
   * Delete associations of the entity.
   *
   * @param id
   * @param associationKey
   * @param associationId
   * @param additionalHeaders
   * @returns {*}
   */
  deleteAssociation (id, associationKey, associationId, additionalHeaders) {
    if (!id || !associationId || !associationId) {
      return Promise.reject(new Error('Missing required arguments.'))
    }

    const headers = this.getBasicHeaders(additionalHeaders)

    return this.httpClient.delete(`${this.getApiBasePath(id)}/${associationKey}/${associationId}`, {
      headers
    }).then((response) => {
      if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response)
      }

      return Promise.reject(response)
    })
  }

  /**
   * Creates a new entity
   *
   * @param {any} payload
   * @param {Object} additionalParams
   * @param {Object} additionalHeaders
   * @returns {Promise<T>}
   */
  create (payload, additionalParams = {}, additionalHeaders = {}) {
    const params = additionalParams
    const headers = this.getBasicHeaders(additionalHeaders)

    return this.httpClient
      .post(this.getApiBasePath(), payload, {
        params,
        headers
      })
      .then((response) => {
        return ApiService.handleResponse(response)
      })
  }

  /**
   * Deletes an existing entity
   *
   * @param {Number} id
   * @param {Object} [additionalParams={}]
   * @param {Object} [additionalHeaders={}]
   * @returns {Promise<T>}
   */
  delete (id, additionalParams = {}, additionalHeaders = {}) {
    if (!id) {
      return Promise.reject(new Error('Missing required argument: id'))
    }

    const params = Object.assign({}, additionalParams)
    const headers = this.getBasicHeaders(additionalHeaders)

    return this.httpClient
      .delete(this.getApiBasePath(id), {
        params,
        headers
      })
  }

  /**
   * Returns the URI to the API endpoint
   *
   * @param {String|Number} [id]
   * @param {String} [prefix='']
   * @returns {String}
   */
  getApiBasePath (id, prefix = '') {
    let url = ''

    if (prefix && prefix.length) {
      url += `${prefix}/`
    }

    if (id && (id.length > 0 || id > 0)) {
      return `${url}${this.apiEndpoint}/${id}`
    }
    return `${url}${this.apiEndpoint}`
  }

  /**
   * Get the basic headers for a request.
   *
   * @param additionalHeaders
   * @returns {Object}
   */
  getBasicHeaders (additionalHeaders = {}) {
    const basicHeaders = {
      Accept: this.contentType,
      // Authorization: `Bearer ${this.loginService.getToken()}`,
      // Language: store.getters.currentLocale,
      // 'Accept-Language': store.getters.currentLocale,
      'Content-Type': 'application/json'
    }

    return Object.assign({}, basicHeaders, additionalHeaders)
  }

  /**
   * Basic response handling.
   * Converts the JSON api data when the specific content type is set.
   *
   * @param response
   * @returns {*}
   */
  static handleResponse (response) {
    if (response.data === null || response.data === undefined) {
      return response
    }

    if (response.data.data === null || response.data.data === undefined) {
      return response.data
    }

    return response.data.data
  }

  /**
   * Parses a JSON api data structure to a simplified object.
   *
   * @param data
   * @returns {Object}
   */
  static parseJsonApiData (data) {
    return parseJsonApi(data)
  }

  static getVersionHeader (versionId) {
    return { 'game-version-id': versionId }
  }

  /**
   * Getter & setter for the API end point
   * @type {String}
   */
  get apiEndpoint () {
    return this.endpoint
  }

  /**
   * @type {String}
   */
  set apiEndpoint (endpoint) {
    this.endpoint = endpoint
  }

  /**
   * Getter & setter for the http client
   *
   * @type {AxiosInstance}
   */
  get httpClient () {
    return this.client
  }

  /**
   * @type {AxiosInstance}
   */
  set httpClient (client) {
    this.client = client
  }

  /**
   * @type {String}
   */
  get contentType () {
    return this.type
  }

  /**
   * @type {String}
   */
  set contentType (contentType) {
    this.type = contentType
  }
}

export default ApiService
