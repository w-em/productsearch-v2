import ApiService from './api.service'

export default ({ $axios }, inject) => {
  class AttributeApiService extends ApiService {
    constructor (apiEndpoint = 'api/attributes') {
      super($axios, apiEndpoint)
      this.name = 'attributeApiService'
    }
  }

  inject('attributeApiService', new AttributeApiService())
}
