import ApiService from './api.service'

export default ({ $axios }, inject) => {
  class ArticleApiService extends ApiService {
    constructor (apiEndpoint = 'api/articles') {
      super($axios, apiEndpoint)
      this.name = 'articleApiService'
    }

    /**
     * Gets a list from the configured API end point using the page & limit.
     *
     * @param {Object} payload
     * @returns {Promise<T>}
     */
    search (payload) {
      const headers = this.getBasicHeaders({})

      return this.httpClient
        .post(this.getApiBasePath('search'), payload, { headers })
        .then((response) => {
          return ApiService.handleResponse(response)
        })
    }

    /**
     * Gets a list from the configured API end point using the page & limit.
     *
     * @param {String} term
     * @param {Array} filters
     * @returns {Promise<T>}
     */
    filter ({
      term,
      filters
    }) {
      const requestHeaders = this.getBasicHeaders({})
      const params = { term, filters }

      return this.httpClient
        .get(this.getApiBasePath('filters'), { params, headers: requestHeaders })
        .then((response) => {
          return ApiService.handleResponse(response)
        })
    }

    /**
     * Gets a list from the configured API end point using the page & limit.
     *
     * @param {String} token
     * @returns {Promise<T>}
     */
    filtersByToken (token) {
      const requestHeaders = this.getBasicHeaders({})
      const params = { token }

      return this.httpClient
        .get(this.getApiBasePath('filtersByToken'), { params, headers: requestHeaders })
        .then((response) => {
          return ApiService.handleResponse(response)
        })
    }

    /**
     * @param {array} slugs
     * @param additionalParams
     * @param additionalHeaders
     * @returns {Promise<never>|Promise<AxiosResponse<any>>}
     */
    getBySlugs (slugs, additionalParams = {}, additionalHeaders = {}) {
      if (slugs.length === 0) {
        return Promise.reject(new Error('Missing required argument: slug'))
      }

      const params = additionalParams
      const headers = this.getBasicHeaders(additionalHeaders)

      return this.httpClient
        .get(this.getApiBasePath('slugs/' + slugs.join(',')), {
          params,
          headers
        })
        .then((response) => {
          return ApiService.handleResponse(response)
        })
    }
  }

  inject('articleApiService', new ArticleApiService())
}
