import ApiService from './api.service'

export default ({ $axios }, inject) => {
  class CategoryApiService extends ApiService {
    constructor (apiEndpoint = 'api/categories') {
      super($axios, apiEndpoint)
      this.name = 'categoryApiService'
    }

    getTree () {
      const requestHeaders = this.getBasicHeaders({})

      return this.httpClient
        .get(this.getApiBasePath('tree'), { headers: requestHeaders })
        .then((response) => {
          return ApiService.handleResponse(response)
        })
    }
  }

  inject('categoryApiService', new CategoryApiService())
}
