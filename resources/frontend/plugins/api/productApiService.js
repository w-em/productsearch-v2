import ApiService from './api.service'

export default ({ $axios }, inject) => {
  class ProductApiService extends ApiService {
    constructor (apiEndpoint = 'api/products') {
      super($axios, apiEndpoint)
      this.name = 'productApiService'
    }
  }

  inject('productApiService', new ProductApiService())
}
