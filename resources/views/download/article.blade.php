<div class="container-fluid">
    <div class="row">
        <div class="col-xs-6 color-green">
            <h1>Technische Daten</h1>
        </div>
        <div class="col-xs-6">
            <h2 style="padding-top: 0">{{ $article->title }}<br/>{{ $article->short_description }}</h2>
            <h3 style="padding-top: 0">Artikelnummer: {{ $article->article_number }}</h3><br/>
        </div>
    </div>
    <div class="row" style="margin-bottom: 60px;">
        <div class="col-xs-6">
            @foreach ($images as $image)
                @if ($image['type'] == 'produktbilder')
                    @if ($loop->first)
                        <img src="{{ $image['path'] }}" class="img-responsive" style="width: 80%">
                    @endif
                @endif
            @endforeach
        </div>
        <div class="col-xs-6">
            <table class="table table-sm">
                <tbody>
                <tr>
                    <th>Verfügbarkeit</th>
                    <td>{{ $article->availability }}</td>
                </tr>
                <tr>
                    <th>Produktmarke</th>
                    <td>{{ $article->vendor }}</td>
                </tr>
                <tr>
                    <th>Produktart</th>
                    <td>{{ $article->categories[0]->title }}</td>
                </tr>
                <tr>
                    <th>EAN Code</th>
                    <td>{{ $article->ean }}</td>
                </tr>
                <tr>
                    <th>Zolltarif Nr.</th>
                    <td>{{ $article->fee_number }}</td>
                </tr>
                <tr>
                    <th>Beschreibung</th>
                    <td>{{ $article->short_description }}</td>
                </tr>
                </tbody>
            </table>
            <div style="padding: 12px;">{{ $article->long_description }}</div>
        </div>
    </div>

    @php ($count = 0)
    @foreach ($article->attributes as $attribute)
        @if ($attribute['group']['id'] === 1)
            @php ($count = $count + 1)
        @endif
    @endforeach

    @if ($count > 0)
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-sm table-condensed table-striped">
                    <thead>
                    <tr>
                        <th colspan="2">Hauptmerkmale</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($article->attributes as $attribute)
                        @if ($attribute['group']['id'] === 1)
                            <tr>
                                <th>{{ $attribute['label'] }}</th>
                                <td>{{ $attribute['value'] }}</td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif

    @php ($count = 0)
    @foreach ($article->attributes as $attribute)
        @if ($attribute['group']['id'] === 2)
            @php ($count = $count + 1)
        @endif
    @endforeach
    @if ($count > 0)
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-sm table-condensed table-striped">
                    <thead>
                    <tr>
                        <th colspan="2">Zusatzmerkmale</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($article->attributes as $attribute)
                        @if ($attribute['group']['id'] === 2)
                            <tr>
                                <th>{{ $attribute['label'] }}</th>
                                <td>{{ $attribute['value'] }}</td>
                            </tr>
                        @endif
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    @endif

    @php ($count = 0)
    @foreach ($article->attributes as $attribute)
        @if ($attribute['group']['id'] === 3)
            @php ($count = $count + 1)
        @endif
    @endforeach

    @if ($count > 0)
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-sm table-condensed table-striped">
                    <thead>
                    <tr>
                        <th colspan="2">Elektrische Daten</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($article->attributes as $attribute)
                        @if ($attribute['group']['id'] === 3)
                            <tr>
                                <th>{{ $attribute['label'] }}</th>
                                <td>{{ $attribute['value'] }}</td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif

    @php ($count = 0)
    @foreach ($article->attributes as $attribute)
        @if ($attribute['group']['id'] === 4)
            @php ($count = $count + 1)
        @endif
    @endforeach

    @if ($count > 0)
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-sm table-condensed table-striped">
                    <thead>
                    <tr>
                        <th colspan="2">Umgebungsbedingungen</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($article->attributes as $attribute)
                        @if ($attribute['group']['id'] === 4)
                            <tr>
                                <th>{{ $attribute['label'] }}</th>
                                <td>{{ $attribute['value'] }}</td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif

    @php ($count = 0)
    @foreach ($article->attributes as $attribute)
        @if ($attribute['group']['id'] === 5)
            @php ($count = $count + 1)
        @endif
    @endforeach
    @if ($count > 0)
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-sm table-condensed table-striped">
                    <thead>
                    <tr>
                        <th colspan="2">Logistikmerkmale</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($article->attributes as $attribute)
                        @if ($attribute['group']['id'] === 5)
                            <tr>
                                <th>{{ $attribute['label'] }}</th>
                                <td>{{ $attribute['value'] }}</td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif

    @php ($count = 0)
    @foreach ($article->attributes as $attribute)
        @if ($attribute['group']['id'] === 6)
            @php ($count = $count + 1)
        @endif
    @endforeach
    @if ($count > 0)
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-sm table-condensed table-striped">
                    <thead>
                    <tr>
                        <th colspan="2">Nachhaltigkeit</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($article->attributes as $attribute)
                        @if ($attribute['group']['id'] === 6)
                            <tr>
                                <th>{{ $attribute['label'] }}</th>
                                <td>{{ $attribute['value'] }}</td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif

    <div class="row" style="margin-top: 60px;margin-bottom: 60px;">
        @foreach ($images as $image)
            @if ($image['type'] == 'maßdarstellung')
                <div class="col-xs-6">
                    <img src="{{ $image['path'] }}" class="img-responsive" style="width: 80%">
                </div>
            @endif
        @endforeach
    </div>

</div>

