<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $article->article_number }}</title>
    <style type="text/css">
        @font-face {
            font-family: 'Roboto';
            src: url({{ $fontPath }}) format("truetype");
            font-weight: normal;
            font-style: normal;
            font-variant: normal;
        }

        {{ $inlineCss }}

    html,
        body {
            font-size: 30px;
            font-family: 'Helvetica', sans-serif;
        }

        @page {
            margin: 200px 150px 500px 150px;
        }


    </style>

</head>
<body>

<header>
    <div class="headline" style="float: left;">{{ $title }}</div>
    <div style="float: right; margin-top: 55px;">Artikelnummer: {{ $article->article_number }}</div>
</header>

<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <b>Haftungsausschluss:</b> Diese Dokumentation dient nicht als Ersatz für die Beurteilung der Eignung oder Verlässlichkeit dieser Produkte für bestimmte Verwendungsbereiche des Benutzers und darf nicht zu diesem Zweck verwendet werden. Irrtümer vorbehalten. <br/><br/>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3">
                <img src="{{ $logoImage }}" class="img-responsive" style="max-width: 600px">
            </div>
            <div class="col-xs-9">
            <span class="pagenum-container" style="padding-right: 10px;">
                <h4 style="margin-right: 30px">{{ $title }} Stand: {{ date("d.m.Y") }} | {{ $article->article_number }} |
                    <span class="pagenum"></span>
                </h4>
            </span>

            </div>
        </div>
    </div>
</footer>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 text-center">
                @foreach ($images as $image)
                    @if ($loop->last)
                        <img src="{{ $image['path'] }}" style="height: 2000px; width: auto; max-width: 100%; margin-top: 100px; margin-left: auto; margin-right: auto;">
                    @else
                        <img src="{{ $image['path'] }}" style="height: 2000px; width: auto; max-width: 100%; margin-top: 300px; margin-left: auto; margin-right: auto;">
                        <div class="page-break"></div>
                    @endif

                @endforeach
            </div>
        </div>
    </div>
</div>
</body>
</html>
